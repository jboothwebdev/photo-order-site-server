from pydantic import BaseModel, Field
from bson import ObjectId
from typing import Optional
from .format_id import PyObjectId


class Orders(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias='_id')
    photoName: str
    orderType: str
    quantity: int
    price: float
    customerId: str
    customerName: str

    class Config(BaseModel):
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
