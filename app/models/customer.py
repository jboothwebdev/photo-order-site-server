from pydantic import BaseModel, Field
from bson import ObjectId
from typing import Optional
from .format_id import PyObjectId


class Customers(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias='_id')
    name: str
    email: str
    streetAddress: str
    city: str
    state: str
    zip: str
    order: Optional[list[str]]

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}


