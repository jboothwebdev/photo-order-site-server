from typing import List
from pydantic import BaseModel
from datetime import datetime, timedelta
from ..models.user import User
from  app import main
from fastapi import APIRouter, status, Depends, HTTPException, Body
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from passlib.context import CryptContext
from jose import JWTError, jwt

# Need to move this to a better location in

SECRET_KEY = 'e393cf53b63f0c47bd0f2c417fdce91f1fa5a318bf3d241fc211a5aeaf7fa258'
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRATION_MINUTES = 120

router = APIRouter(
    prefix="/login",
    tags=["login"]
)


class Token(BaseModel):
    access_token: str
    token_type: str


class UserInDB(User):
    password: str

pwd_context = CryptContext(schemes= ["bcrypt"], deprecated="auto")

oath2_scheme = OAuth2PasswordBearer(tokenUrl='token')

# Verify the
def verify_password(plain_password, hashed_password):
    print(pwd_context.verify(plain_password, hashed_password))
    return pwd_context.verify(plain_password, hashed_password)

def get_password_hash(password):
    return pwd_context.hash(password)


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=30)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


@router.get("/", response_model=List[User])
async def get_users():
    users = []
    cursor = main.db.users.find({})
    async for document in cursor:
        users.append(document)
    return users


@router.post('/signup')
async def create_user(user: User = Body(...)):
    hashed_password = get_password_hash(user.password)
    user.password = hashed_password
    user = jsonable_encoder(user)
    new_user = await main.db.users.insert_one(user)
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=user)


@router.post('/token', response_model=Token)
async def request_token(form_data: OAuth2PasswordRequestForm = Depends()):
    try:
        user = await main.db.users.find_one({"username": form_data.username})
        if not verify_password(form_data.password, user["password"]):
            raise ValueError
        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRATION_MINUTES)
        access_token = create_access_token(
            data={"sub": user["username"]}, expires_delta=access_token_expires
        )
        return {"access_token": access_token, "token_type": "bearer"}
    except ValueError:
        HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
         )




