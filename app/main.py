import os
import motor.motor_asyncio
from fastapi import FastAPI
from .config import MONGODB_URL
from .routers import login
# Import the routers here

# Creating the server

app = FastAPI()
app.include_router(login.router)

client = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URL)
db = client.photoOrderingSite

